FROM openjdk:8-jre-alpine

COPY target/member-service*.jar app/app.jar

WORKDIR /app

CMD ["java", "-jar", "app.jar"]
