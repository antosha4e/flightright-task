## Epic/Story
As a technical lead I want to have a “Member” service so that I can easily:
- Create a new member
- Read an existing member
- Update an existing member
- Delete members which are no longer used
- List existing members
- No frontend needed, only REST services are needed

## Acceptance Criteria
- RESTful Web Service with Spring/Spring Boot
- Member has the following attributes:
    - First name
    - Last name
    - Date of birth
    - Postal code (ZIP)
- Accepts JSON or XML
- Response in JSON or XML
- JDK8/JDK9
- Build with Maven or Gradle
- Data storage: (in memory) database
- Documentation how to start and how to call the service 
