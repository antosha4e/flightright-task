package com.flightright.domain.api;

public interface Member {

    Long getMemberId();
    String getFirstName();
    String getLastName();
    String getDateOfBirth();
    String getPostalCode();
}
