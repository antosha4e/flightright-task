package com.flightright.domain;

import com.flightright.domain.api.Member;
import com.flightright.persistance.entity.MemberEntity;
import com.flightright.persistance.repository.MemberRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class MemberService {

    private final static Logger logger = LoggerFactory.getLogger(MemberService.class);

    private final MemberRepository memberRepository;

    public MemberService(MemberRepository memberRepository) {

        this.memberRepository = memberRepository;
    }

    public List<Member> getAllMembers() {

        return new ArrayList<>(memberRepository.findAll());
    }

    public Member addMember(Member member) {

        return memberRepository.save(new MemberEntity(member));
    }

    public void deleteMemberById(Long memberId) {
        memberRepository.delete(memberId);
    }

    public void updateMember(Member member) {

        memberRepository.save(new MemberEntity(member));
    }

    public Optional<Member> getMemberById(Long memberId) {

        return memberRepository.findById(memberId);
    }
}
