package com.flightright.persistance.entity;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
abstract class Auditable {

    @CreatedDate
    @Column(name = "created_date", updatable = false)
    private long createdDate;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    private long lastModifiedDate;
}
