package com.flightright.persistance.entity;

import com.flightright.domain.api.Member;

import javax.persistence.*;

@Entity
public class MemberEntity extends Auditable implements Member {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "date_of_birth", nullable = false)
    private String dateOfBirth;

    @Column(name = "postal_Code", nullable = false)
    private String postalCode;

    public MemberEntity(){
        super();
    }

    public MemberEntity(final Member member) {

        this.firstName = member.getFirstName();
        this.lastName = member.getLastName();
        this.dateOfBirth = member.getDateOfBirth();
        this.postalCode = member.getPostalCode();
    }

    public Long getMemberId() {
        return id;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    @Override
    public String getPostalCode() {
        return postalCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MemberEntity{");
        sb.append("id=").append(id);
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", dateOfBirth='").append(dateOfBirth).append('\'');
        sb.append(", postalCode='").append(postalCode).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
