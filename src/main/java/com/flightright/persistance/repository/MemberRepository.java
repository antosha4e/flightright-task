package com.flightright.persistance.repository;

import com.flightright.domain.api.Member;
import com.flightright.persistance.entity.MemberEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface MemberRepository extends CrudRepository<MemberEntity, Long> {

    List<MemberEntity> findAll();

    Optional<Member> findById(Long memberId);
}
