package com.flightright.rest.dto;

import com.flightright.domain.api.Member;

import javax.validation.constraints.NotNull;

public class MemberRestDto implements Member {

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private String dateOfBirth;

    @NotNull
    private String postalCode;

    public MemberRestDto(){
        super();
    }

    public MemberRestDto(final Member member) {

        this.firstName = member.getFirstName();
        this.lastName = member.getLastName();
        this.dateOfBirth = member.getDateOfBirth();
        this.postalCode = member.getPostalCode();
    }

    @Override
    public Long getMemberId() {
        return null;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    @Override
    public String getPostalCode() {
        return postalCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("MemberRestDto{");
        sb.append("firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", dateOfBirth='").append(dateOfBirth).append('\'');
        sb.append(", postalCode='").append(postalCode).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
