package com.flightright.rest.controller;

import com.flightright.domain.MemberService;
import com.flightright.domain.api.Member;
import com.flightright.rest.dto.MemberRestDto;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/api/member")
public class MemberController {

    private final static Logger logger = LoggerFactory.getLogger(MemberController.class);

    private final MemberService memberService;

    public MemberController(MemberService memberService) {
        this.memberService = memberService;
    }

    @GetMapping
    @ApiOperation(value = "Fetching all members")
    public List<MemberRestDto> getMembers() {

        logger.info("Fetching all members");

        return memberService.getAllMembers().stream()
                            .map(MemberRestDto::new).collect(toList());
    }

    @GetMapping("/{memberId}")
    @ApiOperation(value = "Get member by Id")
    public ResponseEntity<MemberRestDto> getMemberById(@PathVariable("memberId") Long memberId) {

        logger.info("Get member by Id : {}", memberId);

        return memberService.getMemberById(memberId)
                .map(mem -> new ResponseEntity<>(new MemberRestDto(mem), OK))
                .orElseGet(() -> new ResponseEntity<>(NOT_FOUND));
    }

    @PostMapping
    @ApiOperation(value = "Add new member to the system")
    public ResponseEntity<IdResponse> addMember(@RequestBody @Valid MemberRestDto member) {

        logger.info("Add new member to the system");

        Member addedMember = memberService.addMember(member);

        return new ResponseEntity<>(new IdResponse(addedMember.getMemberId()), CREATED);
    }

    @PutMapping("/{memberId}")
    @ApiOperation(value = "Update member in the system")
    public ResponseEntity<Message> updateMember(@PathVariable("memberId") Long memberId, @RequestBody @Valid MemberRestDto memberToUpdate) {

        logger.info("Update member in the system : {}", memberToUpdate);

        Optional<Member> member = memberService.getMemberById(memberId);

        if(!member.isPresent())
            return new ResponseEntity<>(NOT_FOUND);

        memberService.updateMember(memberToUpdate);

        return new ResponseEntity<>(new Message("Member updated"), OK);
    }

    @DeleteMapping("/{memberId}")
    @ApiOperation(value = "Delete member from the system")
    public ResponseEntity<Message> deleteMember(@PathVariable("memberId") Long memberId) {

        logger.info("Delete member with id - {}", memberId);

        if(!memberService.getMemberById(memberId).isPresent())
            return new ResponseEntity<>(NOT_FOUND);

        memberService.deleteMemberById(memberId);

        return new ResponseEntity<>(new Message("Member deleted"), OK);
    }

    private static class IdResponse {

        Long id;

        IdResponse(Long id) {
            this.id = id;
        }
    }

    private static class Message {
        String msg;

        Message(String msg, Object... args) {
            this.msg = format(msg, args);
        }
    }
}
