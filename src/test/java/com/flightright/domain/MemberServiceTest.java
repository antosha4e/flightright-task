package com.flightright.domain;

import com.flightright.domain.api.Member;
import com.flightright.persistance.entity.MemberEntity;
import com.flightright.persistance.repository.MemberRepository;
import com.flightright.rest.TestMember;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class MemberServiceTest {

    @Mock
    private MemberRepository memberRepositoryMock;

    @InjectMocks
    private MemberService memberService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllMembers() {

        when(memberRepositoryMock.findAll()).thenReturn(Collections.emptyList());

        List<Member> list = memberService.getAllMembers();

        assertNotNull("Member list is null", list);
        assertTrue("Member list is empty", list.isEmpty());
        verify(memberRepositoryMock).findAll();
    }

    @Test
    public void testAddMember() {

        Member member = new TestMember();
        MemberEntity memberEntity = new MemberEntity(member);

        when(memberRepositoryMock.save(refEq(memberEntity))).thenReturn(memberEntity);

        Member returnedMember = memberService.addMember(member);

        assertNotNull("Returned entity null", returnedMember);
        verify(memberRepositoryMock).save(refEq(memberEntity));
    }

    @Test
    public void testDeleteMemberById() {

        Long memberId = 123L;

        doNothing().when(memberRepositoryMock).delete(eq(memberId));

        memberService.deleteMemberById(memberId);

        verify(memberRepositoryMock).delete(eq(memberId));
    }

    @Test
    public void testUpdateMember() {

        Member member = new TestMember();
        MemberEntity memberEntity = new MemberEntity(member);

        when(memberRepositoryMock.save(refEq(memberEntity))).thenReturn(memberEntity);

        memberService.updateMember(member);

        verify(memberRepositoryMock).save(refEq(memberEntity));
    }

    @Test
    public void testGetMemberById_Success() {

        Long memberId = 123L;

        Member member = new TestMember();

        when(memberRepositoryMock.findById(memberId)).thenReturn(Optional.of(member));

        Optional<Member> optionalMember = memberService.getMemberById(memberId);

        assertNotNull("Optional member is null", optionalMember);
        assertTrue("Member is not presented", optionalMember.isPresent());
        assertEquals(member.getLastName(), optionalMember.get().getLastName());
        assertEquals(member.getFirstName(), optionalMember.get().getFirstName());
        assertEquals(member.getPostalCode(), optionalMember.get().getPostalCode());
        assertEquals(member.getDateOfBirth(), optionalMember.get().getDateOfBirth());
        verify(memberRepositoryMock).findById(eq(memberId));
    }

    @Test
    public void testGetMemberById_NotFound() {

        Long memberId = 123L;

        when(memberRepositoryMock.findById(memberId)).thenReturn(Optional.empty());

        Optional<Member> optionalMember = memberService.getMemberById(memberId);

        assertNotNull("", optionalMember);
        assertFalse("", optionalMember.isPresent());
        verify(memberRepositoryMock).findById(eq(memberId));
    }
}
