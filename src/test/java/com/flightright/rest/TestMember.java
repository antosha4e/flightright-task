package com.flightright.rest;

import com.flightright.domain.api.Member;

public class TestMember implements Member {

    private String firstName = "A";
    private String lastName = "A";
    private String dateOfBirth = "A";
    private String postalCode = "A";

    public TestMember() {}

    @Override
    public Long getMemberId() {
        return null;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    @Override
    public String getPostalCode() {
        return postalCode;
    }
}
