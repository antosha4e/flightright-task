package com.flightright.rest.controller;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flightright.domain.MemberService;
import com.flightright.domain.api.Member;
import com.flightright.rest.TestMember;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class MemberControllerTest {

    private MockMvc mvc;

    private ObjectMapper objectMapper;

    private MemberService mockMemberService = mock(MemberService.class);

    @Before
    public void setUp() {

        objectMapper = objectMapper();

        mvc = MockMvcBuilders
                .standaloneSetup(new MemberController(mockMemberService))
                .setMessageConverters(getMessageConverter())
                .build();
    }

    private ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(mapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
        return mapper;
    }

    private HttpMessageConverter<?> getMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();

        converter.setObjectMapper(objectMapper);

        return converter;
    }

    @Test
    public void testGetMembers_Success() throws Exception {

        Member member = new TestMember();

        when(mockMemberService.getAllMembers()).thenReturn(asList(member));

        this.mvc.perform(get("/api/member")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].firstName").value(member.getFirstName()));

        verify(mockMemberService).getAllMembers();
    }

    @Test
    public void testGetMemberById_Success() throws Exception {

        Long memberId = 123L;
        Member member = new TestMember();

        when(mockMemberService.getMemberById(eq(memberId))).thenReturn(Optional.of(member));

        this.mvc.perform(get("/api/member/{memberId}", memberId)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.firstName").value(member.getFirstName()));

        verify(mockMemberService).getMemberById(eq(memberId));
    }

    @Test
    public void testGetMemberById_NotFound() throws Exception {

        when(mockMemberService.getMemberById(anyLong())).thenReturn(Optional.empty());

        this.mvc.perform(get("/api/member/123")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(mockMemberService).getMemberById(anyLong());
    }

    @Test
    public void testAddMember_Success() throws Exception {

        Member memberMock = mock(Member.class);
        Member member = new TestMember();

        when(memberMock.getMemberId()).thenReturn(123L);

        when(mockMemberService.addMember(any(Member.class))).thenReturn(memberMock);

        this.mvc.perform(post("/api/member")
                .content(objectMapper.writeValueAsString(member))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$").isMap())
                .andExpect(jsonPath("$.id").value(memberMock.getMemberId()));

        verify(mockMemberService).addMember(any(Member.class));
    }

    @Test
    public void testUpdateMember_Success() throws Exception {

        Long memberId = 123L;

        Member member = new TestMember();

        when(mockMemberService.getMemberById(eq(memberId))).thenReturn(Optional.of(member));

        doNothing().when(mockMemberService).updateMember(eq(member));

        this.mvc.perform(put("/api/member/{memberId}", memberId)
                .content(objectMapper.writeValueAsString(member))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(mockMemberService).getMemberById(eq(memberId));
        verify(mockMemberService).updateMember(any(Member.class));
    }

    @Test
    public void testUpdateMember_NotFound() throws Exception {

        when(mockMemberService.getMemberById(anyLong())).thenReturn(Optional.empty());

        this.mvc.perform(put("/api/member/123")
                .content(objectMapper.writeValueAsString(new TestMember()))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(mockMemberService).getMemberById(anyLong());
        verify(mockMemberService, never()).updateMember(any());
    }

    @Test
    public void testDeleteMember_Success() throws Exception {

        Long memberId = 123L;

        when(mockMemberService.getMemberById(eq(memberId))).thenReturn(Optional.of(new TestMember()));

        doNothing().when(mockMemberService).deleteMemberById(eq(memberId));

        this.mvc.perform(delete("/api/member/{memberId}", memberId)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(mockMemberService).getMemberById(eq(memberId));
        verify(mockMemberService).deleteMemberById(eq(memberId));
    }

    @Test
    public void testDeleteMember_NotFound() throws Exception {

        when(mockMemberService.getMemberById(anyLong())).thenReturn(Optional.empty());

        this.mvc.perform(delete("/api/member/123")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(mockMemberService).getMemberById(anyLong());
        verify(mockMemberService, never()).deleteMemberById(anyLong());
    }
}
