package com.flightright.persistance.entity;

import com.flightright.domain.api.Member;
import com.flightright.rest.TestMember;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MemberEntityTest {

    @Test
    public void testConstructionOfTheEntity() {

        Member member = new TestMember();

        MemberEntity entity = new MemberEntity(member);

        assertEquals(member.getFirstName(), entity.getFirstName());
        assertEquals(member.getLastName(), entity.getLastName());
        assertEquals(member.getDateOfBirth(), entity.getDateOfBirth());
        assertEquals(member.getPostalCode(), entity.getPostalCode());
    }
}
