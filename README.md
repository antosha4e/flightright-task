# README #

### What is this repository for? ###

Test task for Flightright

Details are [here](TASK.md)

### To build and run project:

``` java

mvn clean package

java -jar target/member-service.jar

```

### Now API available at endpoint:

[http://localhost:9000/api/member](http://localhost:9000/api/member)

### Swagger docs:

[http://localhost:9000/swagger-ui.html](http://localhost:9000/swagger-ui.html)

Areas to improve [here](IMPROVEMENTS.md)